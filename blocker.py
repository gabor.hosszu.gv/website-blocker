from tkinter import *

blocked_sites=[]
padding = 5

WINDOWS_PATH = 'C:\Windows'

def getBlockedSites():
    try:
        hostsfile = open(WINDOWS_PATH + '\System32\drivers\etc\hosts', 'r')
        hosts = hostsfile.readlines()
        blocked_sites=[]
        for line in hosts:
            if line[0] != '#' and line[0] != '\n' and line[0] != ' ':
                blocked_sites.append(line.split())
        #print(blocked_sites)
        hostsfile.close()
        blocked.delete(0, END)
        for site in blocked_sites:
            blocked.insert(END, site)

    except:
        print('Failed to open')

def getTmpSites():
    try:
        hostsfile = open(WINDOWS_PATH + '\System32\drivers\etc\hosts.tmp', 'r')
        hosts = hostsfile.readlines()
        blocked_sites=[]
        for line in hosts:
            if line[0] != '#' and line[0] != '\n' and line[0] != ' ':
                blocked_sites.append(line.split())
        #print(blocked_sites)
        hostsfile.close()
        temp.delete(0, END)
        for site in blocked_sites:
            temp.insert(END, site)
    except:
        print('Failed to open')

def newSite():
    newWin = Toplevel()

    def okPressed():
        newentry = [redirectStr.get(), siteStr.get()]
        hostsfile = open(WINDOWS_PATH + '\System32\drivers\etc\hosts', 'a+')
        hostsfile.write(str(newentry[0]) + ' ' + str(newentry[1] + '\n'))
        hostsfile.close()
        getBlockedSites()
        newWin.destroy()

    siteLabel = Label(newWin, text='Site to block')
    siteLabel.grid(row=0, column=0)

    redirectLabel = Label(newWin, text='Redirect to')
    redirectLabel.grid(row=1, column=0)

    siteStr = StringVar()
    site = Entry(newWin, textvariable = siteStr)
    site.grid(row=0, column=1, pady = padding, padx=padding)

    redirectStr = StringVar()
    redirectStr.set('127.0.0.1')
    redirect = Entry(newWin, textvariable = redirectStr)
    redirect.grid(row=1, column=1, pady = padding, padx=padding)

    okButton = Button(newWin, text='Ok', width=15, command = okPressed)
    okButton.grid(row=2, column=0, columnspan=2, pady = padding, padx=padding)

    site.focus_set()

def removeSite():
    siteRemoval = blocked.get(blocked.curselection())

    hostsfile = open(WINDOWS_PATH + '\System32\drivers\etc\hosts', 'r')
    hosts = hostsfile.readlines()
    hostsfile.close()

    hostsfile = open(WINDOWS_PATH + '\System32\drivers\etc\hosts', 'w')

    for line in hosts:
        if line.split()[0] == siteRemoval[0] and line.split()[1] == siteRemoval[1]:
            pass
        else:
            hostsfile.write(line)

    hostsfile.close()

    tempHostsfile = open(WINDOWS_PATH + '\System32\drivers\etc\hosts.tmp', '+a')
    tempHostsfile.write(siteRemoval[0] + " " + siteRemoval[1] + '\n')
    tempHostsfile.close()

    getBlockedSites()
    getTmpSites()

def killSite():
    try:
        siteRemoval = blocked.get(blocked.curselection())

        hostsfile = open(WINDOWS_PATH + '\System32\drivers\etc\hosts', 'r')
        hosts = hostsfile.readlines()
        hostsfile.close()

        hostsfile = open(WINDOWS_PATH + '\System32\drivers\etc\hosts', 'w')

        for line in hosts:
            if line.split()[0] == siteRemoval[0] and line.split()[1] == siteRemoval[1]:
                pass
            else:
                hostsfile.write(line)

        hostsfile.close()

    except:
        tmpSite = temp.get(temp.curselection())

        tempHostsfile = open(WINDOWS_PATH + '\System32\drivers\etc\hosts.tmp', 'r')
        tempHosts = tempHostsfile.readlines()
        tempHostsfile.close()

        tempHostsfile = open(WINDOWS_PATH + '\System32\drivers\etc\hosts.tmp', 'w')

        for line in tempHosts:
            if line.split()[0] == tmpSite[0] and line.split()[1] == tmpSite[1]:
                pass
            else:
                tempHostsfile.write(line)

        tempHostsfile.close()

    getBlockedSites()
    getTmpSites()

def addSite():
    tmpSite = temp.get(temp.curselection())

    tempHostsfile = open(WINDOWS_PATH + '\System32\drivers\etc\hosts.tmp', 'r')
    tempHosts = tempHostsfile.readlines()
    tempHostsfile.close()

    tempHostsfile = open(WINDOWS_PATH + '\System32\drivers\etc\hosts.tmp', 'w')

    for line in tempHosts:
        if line.split()[0] == tmpSite[0] and line.split()[1] == tmpSite[1]:
            pass
        else:
            tempHostsfile.write(line)

    tempHostsfile.close()

    hostFile = open(WINDOWS_PATH + '\System32\drivers\etc\hosts', '+a')
    hostFile.write(tmpSite[0] + " " + tmpSite[1] + '\n')
    hostFile.close()

    getBlockedSites()
    getTmpSites()

win = Tk()
win.title('Website Blocker')
win.iconbitmap('icon.ico')

blockedLabelText = StringVar()
blockedLabelText.set("Blocked sites")
blockedLabel = Label(win, textvariable=blockedLabelText)
blockedLabel.grid(row=0, column=0)

tempUnblockedLabelText = StringVar()
tempUnblockedLabelText.set("Temporary Unblocked sites")
tempUnblockedLabel = Label(win, textvariable=tempUnblockedLabelText)
tempUnblockedLabel.grid(row=0, column=3)

blocked = Listbox(win, width=50, height=20)
blocked.grid(row=1, column=0, rowspan=10)

temp = Listbox(win, width=50, height=20)
temp.grid(row=1, column=3, rowspan=10)

newButton = Button(win, text='+', width=5, command = newSite)
newButton.grid(row=1, column=2, sticky='N', pady = padding, padx=padding)

removeButton = Button(win, text='->', width=5, command = removeSite)
removeButton.grid(row=2, column=2, sticky='N', pady = padding, padx=padding)

addButton = Button(win, text='<-', width=5, command = addSite)
addButton.grid(row=3, column=2, sticky='N', pady = padding, padx=padding)

killButton = Button(win, text='-', width=5, command = killSite)
killButton.grid(row=4, column=2, sticky='N', pady = padding, padx=padding)

blockedScroll = Scrollbar(win, orient="vertical", command=blocked.yview)
blockedScroll.grid(row=1, column = 1, sticky = 'NS', rowspan=10)
blocked.configure(yscrollcommand=blockedScroll.set)

tempScroll = Scrollbar(win, orient="vertical", command=temp.yview)
tempScroll.grid(row=1, column = 4, sticky = 'NS', rowspan=10)
temp.configure(yscrollcommand=tempScroll.set)

getBlockedSites()
getTmpSites()

win.mainloop()